Feature: Cafe Flora Login
	As a user I wish to login to Cafe Flora using proper credentials and also to access the registration and recover password pages
	
	Scenario Outline: Logging into Cafe Flora
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		Then the user is redirected to home page
	
	Examples:
		| email           | password |
		| email@email.com | password |	
	
	Scenario Outline: Goto the registration page
		Given a user is at the login page of Cafe Flora second time
		When a user clicks the Create a New Account link
		Then the user is redirected to the registration page

	Examples:
		|	|
		|	|
		
	Scenario Outline: Goto the recover password page
		Given a user is at the login page of Cafe Flora third time
		When a user clicks the Forget Password link
		Then the user is redirected to the recover password page

	Examples:
		|	|
		|	|
