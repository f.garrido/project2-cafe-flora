package com.projectcafeflora.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.DistributionDao;
import com.projectcafeflora.model.Distribution;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class DistributionService {

	private DistributionDao distDao;
	
	public Distribution getDistributionById(int id) {
		return distDao.findById(id);
	}
	
}
