package com.projectcafeflora.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.OrderStatusDao;
import com.projectcafeflora.model.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class OrderStatusService {
	
	private OrderStatusDao osDao;
	
	public OrderStatus getById(int orderStatusId) {
		return osDao.findByStatusId(orderStatusId);
	}

}
