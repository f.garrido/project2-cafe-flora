package com.projectcafeflora.service;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.ProjectCafeFloraApplication;
import com.projectcafeflora.dao.CustomerDao;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.MockBank;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class CustomerService {

	 private CustomerDao crDao;
	 private static final String SEED = "mySecretSeed";
	 private MockBankService mbServ;
	 
	 public CustomerService(CustomerDao crDao){
		 super();
		 this.crDao = crDao;
	 }
	 
	 public Customer getById(int customerId) {
		 Customer customer = crDao.findByCustomerId(customerId);
		 String encryptedPassword = customer.getPassword();
		 String decryptedPassword = decrypt(encryptedPassword);
		 customer.setCreditCardNumber(decrypt(customer.getCreditCardNumber()));
		 return customer;
	 }
	 
	 //extra for remember email or password just incase
	 public String retreiveEmailByPhone(String phone) {
		 return crDao.findByPhone(phone).getEmail();
	 }
	 	 
	 public Customer viewProfile(String email, String password) {
		 String tempEmail = email.toLowerCase();
		 Customer customer = null;
		 try {
			 customer = crDao.findByEmail(tempEmail);
			 String encryptedPassword = customer.getPassword();
			 String decryptedPassword = decrypt(encryptedPassword);
			 customer.setCreditCardNumber(decrypt(customer.getCreditCardNumber()));
			 if (decryptedPassword == null) {
				 //log failed login attempt	
				 return null;
				}
			 if (password.equals(decryptedPassword)) {
				 ProjectCafeFloraApplication.log.info("Login succeeded for email: " + tempEmail);
				return customer;
			 } else {
				 ProjectCafeFloraApplication.log.info("Login failed for email: " + tempEmail);
				return null;
			 }
		 }catch(NullPointerException e) {
			 ProjectCafeFloraApplication.log.info("Login failed for email: " + tempEmail);
			 return null;
		 }
	 }
	 
//	 public Customer viewByEmail(String email) {
//		 String tempEmail = email.toLowerCase();
//		 Customer customer = crDao.findByEmail(tempEmail);
//		 return customer;
//	 }
	 
	 public void changePassword(String email, String password) {
		 String tempEmail = email.toLowerCase();
		 Customer customer = crDao.findByEmail(tempEmail);
		 customer.setPassword(encrypt(password));
		 crDao.save(customer);
	 }
	 
	 //creating new customer account
	 public void insertCustomer(String email, String password, String firstName, String lastName, String address, String city, String state, String zipCode, 
			 String phone, String creditCardNumber, String creditCardName, int creditCardCode, int creditCardMonth, int creditCardYear) {
		 Customer customer = new Customer();
		 customer.setEmail(email.toLowerCase());
		 customer.setPassword(encrypt(password));
		 customer.setFirstName(firstName);
		 customer.setLastName(lastName);
		 customer.setAddress(address);
		 customer.setCity(city);
		 customer.setState(state);
		 customer.setZipCode(zipCode);
		 customer.setPhone(phone);
		 customer.setCreditCardNumber(encrypt(creditCardNumber));
		 customer.setCreditCardName(creditCardName);
		 customer.setCreditCardCode(creditCardCode);
		 customer.setCreditCardMonth(creditCardMonth);
		 customer.setCreditCardYear(creditCardYear);
		 ProjectCafeFloraApplication.log.info("Login failed for email: " + customer.getEmail());
		 crDao.save(customer);
	 }
	 
	 //updating customer's address
	 //can can be simply by replacing email and password by customer_id after they log in but for now I am doing this like this
	 public void updateAddress(String email, String password, String address, String city, String state, String zipCode) {
		 Customer customer = crDao.findByEmailAndPassword(email, password);
		 customer.setAddress(address);
		 customer.setCity(city);
		 customer.setState(state);
		 customer.setZipCode(zipCode);
		 crDao.save(customer);		 
	 }
	
	 //updating customer's credit card
	 public void updateCreditCard(String email, String password, String creditCardNumber, String creditCardName, int creditCardCode, int creditCardMonth, int creditCardYear){
		 Customer customer = crDao.findByEmailAndPassword(email, password);
		 customer.setCreditCardNumber(encrypt(creditCardNumber));
		 customer.setCreditCardName(creditCardName);
		 customer.setCreditCardCode(creditCardCode);
		 customer.setCreditCardMonth(creditCardMonth);
		 customer.setCreditCardYear(creditCardYear);
		 crDao.save(customer);
	 }
	 
	 public void deleteCustomer(String email, String password) {
		 Customer customer = crDao.findByEmailAndPassword(email, password);
		 crDao.delete(customer);
	 }
	 
	 //compare client credit card to bank credit card - return true if same, false if different
	 public String checkCreditCard(String creditCardNumber, String creditCardName, int creditCardCode, int creditCardMonth, int creditCardYear, double total) {
		 if (mbServ.getCreditCardByNumber(creditCardNumber) == null) {
			 return "rejected";
		 } else {
			 MockBank mb = mbServ.getCreditCardByNumber(creditCardNumber);
			 if (mb.getCreditCardNumber().equals(creditCardNumber) && mb.getCreditCardName().equals(creditCardName) &&
					 mb.getCreditCardCode() == creditCardCode && mb.getCreditCardMonth() == creditCardMonth &&
					 mb.getCreditCardYear() == creditCardYear) {
				 if (mb.getCreditAmount() >= total) {
					 return "accepted";
				 } else {
					 return "insufficent";
				 }
			 } else {
				 return "rejected";
			 }
		 }
	 }
	 
	 //dont forget to do a test for this one
	 public String retrievePassword(String email, String firstName, String lastName) {
		 Customer customer = crDao.findByEmail(email);
		 if(customer.equals(null)) {
			 throw new NullPointerException();
		 }
		 if(customer.getFirstName().equals(firstName) && customer.getLastName().equals(lastName)) {
			 EmailService.sendPasswordEmail(decrypt(customer.getPassword()), firstName, lastName, email);
			 return "Email sent";
		 }else {
			 return "Unauthorized user wrong firstname or lastname";
		 }
	 }
	 
		public String encrypt(String passWordIn) {
			StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
			encryptor.setPassword(SEED);
			String encryptedPassword = encryptor.encrypt(passWordIn);
			return encryptedPassword;
		}
		
		public String decrypt(String passWordIn) {
			StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
			encryptor.setPassword(SEED);
			String decryptedPassword = encryptor.decrypt(passWordIn);
			return decryptedPassword;
		}

}


