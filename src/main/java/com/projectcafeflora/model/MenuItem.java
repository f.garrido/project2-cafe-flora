package com.projectcafeflora.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor
@ToString
@Entity
@Table(name="menu_item")
public class MenuItem implements Serializable {

	@Id
	@Column(name="item_id")
	@Setter(AccessLevel.NONE)
	private int itemId;
	
	@Column(name="item_name", nullable=false)
	private String itemName;
	
	@Column(name="item_description", nullable=false)
	private String itemDescription;
	
	@Column(name="item_cost", nullable=false)
	private double itemCost;
	
	@Column(name="item_image")
	private String itemImageUrl;

	public MenuItem(String itemName, String itemDescription, double itemCost, String itemImageUrl) {
		super();
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemCost = itemCost;
		this.itemImageUrl = itemImageUrl;
	}
	
	public boolean equals(MenuItem mi) {
		if (this.getItemId() == mi.getItemId() && this.getItemName().equals(mi.getItemName()) && 
			this.getItemDescription().equals(mi.getItemDescription()) && this.getItemCost() == mi.getItemCost() &&
			this.getItemImageUrl().equals(mi.getItemImageUrl())) {
			return true;
		} else {
			return false;
		}
	}
		
	
}
