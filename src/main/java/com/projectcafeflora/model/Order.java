package com.projectcafeflora.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="customer_order")
public class Order implements Serializable {
	
	@Id
	@Column(name="order_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
	private int orderId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="status_id")
	private OrderStatus orderStatus;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="distribution_id")
	private Distribution distribution;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="coupon_code")
	private Coupon coupon;
	
	@Column(name="subtotal")
	private double subtotal;
	
	@Column(name="tax")
	private double tax;
	
	@Column(name="delivery_tip")
	private double deliveryTip;
	
	@Column(name="total")
	private double total;
	
	@Column(name="ordered_timestamp")
	private Timestamp orderedTimestamp;
	
	@Column(name="preparing_timestamp")
	private Timestamp preparingTimestamp;
	
	@Column(name="ready_timestamp")
	private Timestamp readyTimestamp;
	
	@Column(name="completed_timestamp")
	private Timestamp completedTimestamp;
	
	public Order(Customer customer, OrderStatus orderStatus, Distribution distribution, Coupon coupon, double subtotal,
			double tax, double deliveryTip, double total, Timestamp orderedTimestamp, Timestamp preparingTimestamp,
			Timestamp readyTimestamp, Timestamp completedTimestamp) {
		super();
		this.customer = customer;
		this.orderStatus = orderStatus;
		this.distribution = distribution;
		this.coupon = coupon;
		this.subtotal = subtotal;
		this.tax = tax;
		this.deliveryTip = deliveryTip;
		this.total = total;
		this.orderedTimestamp = orderedTimestamp;
		this.preparingTimestamp = preparingTimestamp;
		this.readyTimestamp = readyTimestamp;
		this.completedTimestamp = completedTimestamp;
	}

	public Order(Customer customer, OrderStatus orderStatus, double subtotal) {
		super();
		this.customer = customer;
		this.orderStatus = orderStatus;
		this.subtotal = subtotal;
		this.tax = 0.0;
		this.deliveryTip = 0.0;
		this.total = 0.0;
	}
	
	
	
}

