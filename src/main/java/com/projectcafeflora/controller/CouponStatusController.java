package com.projectcafeflora.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.service.CouponStatusService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/couponstatuses")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class CouponStatusController {
	
	private CouponStatusService csServ;
	
	@GetMapping("/{coupstatid}")
	public ResponseEntity<CouponStatus> getCouponStatusById(@PathVariable("coupstatid") String couponStatusId){
		
		int id = Integer.parseInt(couponStatusId);
		if(csServ.getById(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(csServ.getById(id), HttpStatus.OK);
	}
	
	

}
