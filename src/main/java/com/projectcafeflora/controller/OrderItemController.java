package com.projectcafeflora.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.OrderItem;
import com.projectcafeflora.service.OrderItemService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/orderitems")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class OrderItemController {
	
	private OrderItemService oiServ = new OrderItemService();
	
	@PostMapping()
	public ResponseEntity<String> insertMenuItem(@RequestBody LinkedHashMap<String, String> xMap) {
		int orderId = Integer.parseInt(xMap.get("orderId"));
		int menuItemId = Integer.parseInt(xMap.get("menuItemId"));
		int quantity = Integer.parseInt(xMap.get("quantity"));
		String specialInstructions = xMap.get("specialInstructions");
		oiServ.insertOrderItem(orderId, menuItemId, quantity, specialInstructions);
		return new ResponseEntity<>("Order Item Created", HttpStatus.CREATED);
	}
	
	@PutMapping()
	public ResponseEntity<String> updateMenuItem(@RequestBody LinkedHashMap<String, String> xMap) {
		int orderItemId = Integer.parseInt(xMap.get("orderItemId"));
		int quantity = Integer.parseInt(xMap.get("quantity"));
		String specialInstructions = xMap.get("specialInstructions");
		oiServ.updateOrderItem(orderItemId, quantity, specialInstructions);
		return new ResponseEntity<>("Order Item Updated", HttpStatus.OK);
	}

	@GetMapping("/individualorderitems/{orderitemid}")
	public ResponseEntity<OrderItem> getOrderItemById(@PathVariable("orderitemid") String orderItemId) {
		int id = Integer.parseInt(orderItemId);
		if (oiServ.getById(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(oiServ.getById(id), HttpStatus.OK);
	}
	
	@GetMapping("/orderorderitems/{orderid}")
	public ResponseEntity<List<OrderItem>> getOrderItemByOrderId(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		if (oiServ.getByOrderId(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(oiServ.getByOrderId(id), HttpStatus.OK);
	}
	
	@GetMapping("/individualordermenuitems/{orderid}/{menuitem}")
	public ResponseEntity<OrderItem> getOrderItemByOrderAndMenuId(@PathVariable("orderid") String orderId, @PathVariable("menuitem") String menuItem) {
		int oId = Integer.parseInt(orderId);
		int mId = Integer.parseInt(menuItem);
		if (oiServ.getByOrderAndMenuItem(oId, mId) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(oiServ.getByOrderAndMenuItem(oId, mId), HttpStatus.OK);
	}
	
	@DeleteMapping("/orderitems/{orderitemid}")
	public ResponseEntity<String> deleteOrderItemById(@PathVariable("orderitemid") String orderItemId) {
		oiServ.deleteById(Integer.parseInt(orderItemId));
		return new ResponseEntity<>("\"Order Item is Deleted\"", HttpStatus.OK);
	}
	
	@DeleteMapping("/orders/{orderid}")
	public ResponseEntity<String> deleteOrderItemByOrder(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		List<OrderItem> oiList = oiServ.getByOrderId(id);
		for (OrderItem item : oiList) {
			oiServ.deleteById(item.getOrderItemId());
		}
		return new ResponseEntity<>("Order Item(s) is Deleted", HttpStatus.OK);
	}

}
