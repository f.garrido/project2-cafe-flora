package com.projectcafeflora.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.Customer;
import com.projectcafeflora.service.CustomerService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/customer")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class CustomerController {
	
	private CustomerService crServ = new CustomerService();
	

	@PostMapping("/register")
	public ResponseEntity<String> createCustomer(@RequestBody LinkedHashMap<String, String> crMap){
		String email = crMap.get("email");
		String password = crMap.get("password");
		String firstName = crMap.get("firstName");
		String lastName = crMap.get("lastName");
		String address = crMap.get("address");
		String city = crMap.get("city");
		String state = crMap.get("state");
		String zipCode = crMap.get("zipCode");
		String phone = crMap.get("phone");
		String creditCardNumber = crMap.get("creditCardNumber");
		String creditCardName = crMap.get("creditCardName");
		int creditCardCode = 0;
		int creditCardMonth = 0;
		int creditCardYear =0;
		try {
			creditCardCode = Integer.parseInt(crMap.get("creditCardCode"));
		} catch(NumberFormatException e) {
		}
		try {
			creditCardMonth = Integer.parseInt(crMap.get("creditCardMonth"));
		} catch(NumberFormatException e) {
		}
		try {
			creditCardYear = Integer.parseInt(crMap.get("creditCardYear"));
		} catch(NumberFormatException e) {
		}
		crServ.insertCustomer(email, password, firstName, lastName, address, city, state, zipCode, phone, creditCardNumber, creditCardName, creditCardCode, creditCardMonth, creditCardYear);
		return new ResponseEntity<>("Customer account created", HttpStatus.CREATED);
	}
	
/*
 	//I commented this for now until I read EmailService
	//change this to PutMapping after you study about putMapping 
	@PostMapping("/changePassword")
	public ResponseEntity<String> updatePassword(@RequestBody LinkedHashMap<String, String> crMap){
		String email = crMap.get("email");
		String password = crMap.get("password");
		crServ.changePassword(email, password);
		return new ResponseEntity<>("Customer password updated", HttpStatus.ACCEPTED);
	}
*/	

	@PostMapping("/address")
	public ResponseEntity<String> updateAddress(@RequestBody LinkedHashMap<String, String> crMap){
		String email = crMap.get("email");
		String password = crMap.get("password");
		String address = crMap.get("address");
		String city = crMap.get("city");
		String state = crMap.get("state");
		String zipCode = crMap.get("zipCode");
		crServ.updateAddress(email, password, address, city, state, zipCode);
		return new ResponseEntity<>("Customer address updated", HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/creditcard")
	public ResponseEntity<String> updateCreditCard(@RequestBody LinkedHashMap<String, String> crMap){
		String email = crMap.get("email");
		String password = crMap.get("password");
		String creditCardNumber = crMap.get("creditCardNumber");
		String creditCardName = crMap.get("creditCardName");
		int creditCardCode = Integer.parseInt(crMap.get("creditCardCode"));
		int creditCardMonth = Integer.parseInt(crMap.get("creditCardMonth"));
		int creditCardYear = Integer.parseInt(crMap.get("creditCardYear"));
		crServ.updateCreditCard(email, password, creditCardNumber, creditCardName, creditCardCode, creditCardMonth, creditCardYear);
		return new ResponseEntity<>("Customer credit card updated", HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/account")
	public ResponseEntity<Customer> getCustomerAccount(@RequestBody LinkedHashMap<String, String> crMap){
		String email = crMap.get("email");
		String password = crMap.get("password");
		Customer customer = crServ.viewProfile(email, password);
		if(customer == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(customer, HttpStatus.OK );
	}
	
	@GetMapping("/account/{id}")
	public ResponseEntity<Customer> getAccountById(@PathVariable("id") int customerId){
		if(crServ.getById(customerId) ==null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(crServ.getById(customerId), HttpStatus.OK );
	}
	
	@DeleteMapping("/removeAccount")
	public ResponseEntity<String> removeCustomer(@RequestBody LinkedHashMap<String, String> crMap){
		String email = crMap.get("email");
		String password = crMap.get("password");
		crServ.deleteCustomer(email, password);
		return new ResponseEntity<>("Customer account deleted" , HttpStatus.OK);
	}
	
	//this method gets cc info and sends it to the service where it will be compared and validated
	@PostMapping("/verifycreditcards")
	public ResponseEntity<String> verifyCC(@RequestBody LinkedHashMap<String, String> crMap){
		String creditCardNumber = crMap.get("creditCardNumber");
		String creditCardName = crMap.get("creditCardName");
		int creditCardCode = 0;
		int creditCardMonth = 0;
		int creditCardYear = 0;
		try {
			creditCardCode = Integer.parseInt(crMap.get("creditCardCode"));
		} catch(NumberFormatException e) {
			creditCardCode = 0;
		}
		try {
			creditCardMonth = Integer.parseInt(crMap.get("creditCardMonth"));
		} catch(NumberFormatException e) {
			creditCardCode = 0;
		}
		try {
			creditCardYear = Integer.parseInt(crMap.get("creditCardYear"));
		} catch(NumberFormatException e) {
			creditCardCode = 0;
		}
		double total = Double.parseDouble(crMap.get("total"));
		String verified = crServ.checkCreditCard(creditCardNumber, creditCardName, creditCardCode, creditCardMonth, creditCardYear, total);
		return new ResponseEntity<>(verified, HttpStatus.OK );
	}
	
	@PostMapping("/retrievepassword")
	public ResponseEntity<String> retrieveUserPassword(@RequestBody LinkedHashMap<String, String> crMap){
		String firstName = crMap.get("firstName");
		String lastName = crMap.get("lastName");
		String email = crMap.get("email");
		try {
			String response = crServ.retrievePassword(email, firstName, lastName);
			if(response.equals("Email sent")){
				return new ResponseEntity<>(response, HttpStatus.OK);
			}return new ResponseEntity<>(response, HttpStatus.CONFLICT);
		}catch(NullPointerException e) {
			return new ResponseEntity<>("Email not found", HttpStatus.BAD_REQUEST);
		}
	}
}
