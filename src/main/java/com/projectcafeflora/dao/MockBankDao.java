package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.MockBank;

public interface MockBankDao extends JpaRepository<MockBank, Integer> {
	
	// Possibly find all of MockBank
	public List<MockBank> findAll();
	
	// CreditCardNumber
	public MockBank findByCreditCardNumber(String creditCardNumber);
	
	// CreditAmount
	public List<MockBank> findByCreditAmount(double balance);
	
	// CreditCardName
	public MockBank findByCreditCardName(String CreditCardName);
	
	// CreditCardCode
	public List<MockBank> findByCreditCardCode(int CreditCardCode);
	
}
