package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.Employee;

public interface EmployeeDao extends JpaRepository<Employee, Integer>{
	
	public List<Employee> findAll();
	public Employee findById(int id);
	public Employee findByEmail(String email);
	//Login functionality
	public Employee findByEmailAndPassword(String email, String password);

}
