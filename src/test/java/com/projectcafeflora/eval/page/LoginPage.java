package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	private WebDriver driver;

	@FindBy(xpath = "//*[@id='greeting']")
	private WebElement greeting;
	
	@FindBy(xpath = "//*[@type='email']")
	private WebElement email;
	
	@FindBy(xpath = "//*[@type='password']")
	private WebElement password;
	
	@FindBy(xpath = "//*[@type='submit']")
	private WebElement loginButton;
	
	@FindBy(xpath = "//*[@id='register']")
	private WebElement registerButton;
	
	@FindBy(xpath = "//*[@id='forget']")
	private WebElement recoverPasswordButton;
	
	@FindBy(xpath = "//*[@id='message']")
	private WebElement message;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void setEmail(String email) {
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}
	
	public String getMessage() {
		return this.message.getText();
	}
	
	public void login() {
		this.loginButton.click();
	}
	
	public void register() {
		this.registerButton.click();
	}
	
	public void recoverPassword() {
		this.recoverPasswordButton.click();
	}
	
	public String getGreeting() {
		return this.greeting.getText();
	}
	
}
