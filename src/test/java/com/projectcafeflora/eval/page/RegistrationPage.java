package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class RegistrationPage {
	
	private WebDriver driver;
	
	@FindBy(xpath="//*[@id='title']")
	public WebElement title;
	
	@FindBy(xpath="//*[@id='firstName']")
	public WebElement firstName;
	
	@FindBy(xpath="//*[@id='lastName']")
	public WebElement lastName;
	
	@FindBy(xpath="//*[@id='email']")
	public WebElement email;

	@FindBy(xpath="//*[@id='password']")
	public WebElement password;
	
	@FindBy(xpath="//*[@id='address']")
	public WebElement address;
	
	@FindBy(xpath="//*[@id='city']")
	public WebElement city;
	
	@FindBy(xpath="//*[@id=\"state\"]")
	public WebElement state;
	
	@FindBy(xpath="//*[@id='zipCode']")
	public WebElement zipCode;
	
	@FindBy(xpath="//*[@id='phone']")
	public WebElement phone;
	
	@FindBy(xpath="//*[@id='creditCardName']")
	public WebElement creditCardName;
	
	@FindBy(xpath="//*[@id='creditCardNumber']")
	public WebElement creditCardNumber;
	
	@FindBy(xpath="//*[@id='creditCardMonth']")
	public WebElement creditCardMonth;
	
	@FindBy(xpath="//*[@id='creditCardYear']")
	public WebElement creditCardYear;
	
	@FindBy(xpath="//*[@id='creditCardCode']")
	public WebElement creditCardCode;
	
	@FindBy(xpath="/html/body/app-root/app-register/div/form/div[10]/button")
	public WebElement registerButton;
	
	@FindBy(xpath="/html/body/app-root/app-register/div/form/div[10]/a")
	public WebElement loginLink;
	
	public RegistrationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
//	public void registerAnAccount(String firstName, String lastName, String email, String password, String address, String city,
//			String state, String zipCode, String phone, String creditCardName, String creditCardNumber, int creditCardMonth,
//			int creditCardYear, int creditCardCode) {
//		this.firstName.sendKeys(firstName);
//		this.lastName.sendKeys(lastName);
//		this.email.sendKeys(email);
//		this.password.sendKeys(password);
//		this.address.sendKeys(address);
//		this.city.sendKeys(city);
//		this.dropState.selectByValue(state);
//		this.zipCode.sendKeys(zipCode);
//		this.phone.sendKeys(phone);
//		this.creditCardName.sendKeys(creditCardName);
//		this.creditCardNumber.sendKeys(creditCardNumber);
//		this.dropMonth.selectByValue(String.valueOf(creditCardMonth));
//		this.dropYear.selectByValue(String.valueOf(creditCardYear));
//		this.creditCardYear.sendKeys(String.valueOf(creditCardCode));
//		this.registerButton.click();
//	}
	
	public void setFirstName(String string) {
		this.firstName.clear();
		this.firstName.sendKeys(string);
	}
	
	public void setLastName(String string) { 
		this.lastName.clear();
		this.lastName.sendKeys(string);
	}
	
	public void setEmail(String string) {
		this.email.clear();
		this.email.sendKeys(string);
	}
	
	public void setPassword(String string) {
		this.password.clear();
		this.password.sendKeys(string);
	}
	
	public void setAddress(String string) {
		this.address.clear();
		this.address.sendKeys(string);
	}
	
	public void setCity(String string) {
		this.city.clear();
		this.city.sendKeys(string);
	}
	
	public void setState(String string) {
		Select dropState = new Select(state);
		dropState.selectByValue(string);
	}
	
	public void setZipCode(String string) {
		this.zipCode.clear();
		this.zipCode.sendKeys(string);
	}
	
	public void setPhone(String string) {
		this.phone.clear();
		this.phone.sendKeys(string);
	}
	
	public void setCreditCardName(String string) {
		this.creditCardName.clear();
		this.creditCardName.sendKeys(string);
	}
	
	public void setCreditCardNumber(String string) {
		this.creditCardNumber.clear();
		this.creditCardNumber.sendKeys(string);
	}
	
	public void setCreditCardMonth(int number) {
		Select dropMonth = new Select(creditCardMonth);
		dropMonth.selectByValue(String.valueOf(number));
	}
	
	public void setCreditCardYear(int number) {
		Select dropYear = new Select(creditCardYear);
		dropYear.selectByValue(String.valueOf(number));
	}
	
	public void setCreditCardCode(int number){
		this.creditCardCode.clear();
		this.creditCardCode.sendKeys(String.valueOf(number));
	}
	
	public String getTitle() {
		return this.title.getText();
	}
	
	public void clickRegister() {
		this.registerButton.click();
	}
	
	public void goBackToLogin(){
		this.loginLink.click();
	}
}
