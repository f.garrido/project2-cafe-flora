package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	private WebDriver driver;
	
	@FindBy(xpath ="//*[@id='header']")
	private WebElement navBarHeader;
	
	@FindBy(xpath = "//*[@type='email']")
	private WebElement email;
	
	@FindBy(xpath = "//*[@type='password']")
	private WebElement password;
	
	@FindBy(xpath = "//*[@type='submit']")
	private WebElement loginButton;
	
	@FindBy(xpath ="//*[@id='menu']")
	private WebElement menuButton;
	
	@FindBy(xpath ="//*[@id='home']")
	private WebElement homeButton;
	
	@FindBy(xpath ="//*[@id='cart']")
	private WebElement cartButton;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void setEmail(String email) {
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}
	
	public String getNavBarHeader() {
		return navBarHeader.getText();
	}
	
	public void clickMenuButton() {
		this.menuButton.click();
	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickCartButton() {
		this.cartButton.click();
	}
}
