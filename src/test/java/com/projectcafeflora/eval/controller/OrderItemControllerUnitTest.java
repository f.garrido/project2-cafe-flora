package com.projectcafeflora.eval.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.OrderItemController;
import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Distribution;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderItem;
import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderItemService;

@SpringBootTest
public class OrderItemControllerUnitTest {

	@Mock
	private OrderItemService oServ;

	@InjectMocks
	private OrderItemController oCon;

	@InjectMocks
	private OrderItemController oCon2 = mock(OrderItemController.class);

	private Customer customer;
	private OrderStatus orderStatus;
	private Coupon coupon;
	private CouponStatus couponStatus;
	private Distribution distribution;
	private Timestamp timestamp;

	private MenuItem menuItem;
	private MenuItem menuItem2;
	private MenuItem menuItem3;

	private Order order;
	private OrderItem orderItem;
	private OrderItem orderItem2;
	private OrderItem orderItem3;
	private List<OrderItem> orderItemList = new ArrayList<>();
	private List<OrderItem> emptyList = new ArrayList<>();

	private MockMvc mock;

	@BeforeEach
	public void setUp() throws Exception {
		timestamp = new Timestamp(System.currentTimeMillis());
		distribution = new Distribution(1, "Delivery");
		couponStatus = new CouponStatus(1, "Active");
		coupon = new Coupon("pancakespecial", menuItem, couponStatus, -1.00);
		orderStatus = new OrderStatus(1, "Building");
		customer = new Customer(1, "test@email.com", "pass", "Test", "Customer", "123 street", "Fakesville", "Iowa",
				"123456", "1231234567", "9876598765987650", "Test Customer", 999, 1, 20125);
		order = new Order(1, customer, orderStatus, distribution, coupon, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp,
				timestamp, timestamp);

		menuItem = new MenuItem(1, "Pancakes", "Stack of fluffy pancakes covered in butter and syrup", 3.00, null);
		menuItem2 = new MenuItem(2, "Spinach Wrap", "Turkey, bacon, avocado, lettuce, and tomato in a spinach tortilla",
				6.50, null);
		menuItem3 = new MenuItem(3, "Chicken Tenders", "Mouthwatering chicken tenders, fries included", 8.50, null);

		orderItem = new OrderItem(1, order, menuItem, 1, "extra syrup");
		orderItem2 = new OrderItem(2, order, menuItem2, 1, null);
		orderItem3 = new OrderItem(3, order, menuItem3, 1, null);

		orderItemList.add(orderItem);
		orderItemList.add(orderItem2);
		orderItemList.add(orderItem3);

		orderItemList.add(orderItem);
		orderItemList.add(orderItem2);
		orderItemList.add(orderItem3);

		mock = MockMvcBuilders.standaloneSetup(oCon).build();

		when(oServ.getById(orderItem.getOrderItemId())).thenReturn(orderItem);
		when(oServ.getByOrderId(order.getOrderId())).thenReturn(orderItemList);
		when(oServ.getByOrderAndMenuItem(order.getOrderId(), menuItem.getItemId())).thenReturn(orderItem);
		doNothing().when(oServ).deleteByOrderId(order.getOrderId());
		doNothing().when(oServ).deleteById(orderItem.getOrderItemId());
		doNothing().when(oServ).insertOrderItem(order.getOrderId(), menuItem.getItemId(), orderItem.getQuantity(),
				orderItem.getSpecialInstructions());
		doNothing().when(oServ).updateOrderItem(orderItem.getOrderItemId(), orderItem.getQuantity(), orderItem.getSpecialInstructions());
	}

	@Test
	public void InsertMenuItemTest() throws Exception {
		mock.perform(post("/orderitems").contentType(MediaType.APPLICATION_JSON)
			.content("{\"orderId\":\"1\",\"menuItemId\":\"1\",\"quantity\":\"1\",\"specialInstructions\":\"extra syrup\"}"))
			.andExpect(status().isCreated()).andExpect(jsonPath("$").value("Order Item Created"));
	}

	@Test
	public void updateMenuItemTest() throws Exception {
		mock.perform(put("/orderitems").contentType(MediaType.APPLICATION_JSON)
				.content("{\"orderItemId\":\"1\",\"quantity\":\"1\",\"specialInstructions\":\"extra syrup\"}"))
				.andExpect(status().isOk()).andExpect(jsonPath("$").value("Order Item Updated"));
		}

	@Test
	public void getOrderItemByIdTest() throws Exception {
		mock.perform(get("/orderitems/individualorderitems/{orderitemid}", orderItem.getOrderItemId()))
				.andExpect(status().isOk());
	}

	@Test
	public void getOrderItemByOrderIdTest() throws Exception {
		mock.perform(get("/orderitems/orderorderitems/{orderid}", order.getOrderId())).andExpect(status().isOk());
	}

	@Test
	public void getOrderItemByOrderAndMenuIdTest() throws Exception {
		Integer[] oAndM = new Integer[] { order.getOrderId(), menuItem.getItemId() };
		mock.perform(get("/orderitems/individualordermenuitems/{orderid}/{menuitem}", oAndM))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteOrderItemByIdTest() throws Exception {
		mock.perform(delete("/orderitems/orderitems/{orderitemid}", orderItem.getOrderItemId()))
				.andExpect(status().isOk()).andExpect(jsonPath("$").value("Order Item is Deleted"));
	}

	@Test
	public void deleteOrderItemByOrderTest() throws Exception {
		mock.perform(delete("/orderitems/orders/{orderid}", order.getOrderId())).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Order Item(s) is Deleted"));

	}
}
