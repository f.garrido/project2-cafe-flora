package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.OrderController;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderService;

@SpringBootTest
public class OrderControllerUnitTest {
	
	@Mock
	private OrderService oServ;
	
	@InjectMocks
	private OrderController oCon;
	
	private List<Order> orderList = new ArrayList<Order>();
	private MockMvc mock;
	
	private Order order, order2;
	private Customer customer;
	private OrderStatus orderStatus;
	
	@BeforeEach
	public void setUp() throws Exception{
		
		orderStatus = new OrderStatus(1, "Building");
		customer = new Customer(1, "email@email.com", "password", "John", "Doe", "", "", "", "", "5555555555", "", "", 0, 0, 0);		
		order = new Order(customer, orderStatus, 1.0);
		order2 = new Order(customer, orderStatus, 2.0);
		
		orderList.add(order);		
		orderList.add(order2);
		
		mock = MockMvcBuilders.standaloneSetup(oCon).build();	
	}
	
	@Test
	public void testgetAllOrders() throws Exception{
		when(oServ.getAll()).thenReturn(orderList);
		
		mock.perform(get("/orders/allorders"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.[0].orderId", is(order.getOrderId())))
		.andExpect(jsonPath("$.[0].subtotal", is(order.getSubtotal())))
		.andExpect(jsonPath("$.[1].orderId", is(order2.getOrderId())))
		.andExpect(jsonPath("$.[1].subtotal", is(order2.getSubtotal())));
	}
	
	@Test
	public void testGetByOrderStatus() throws Exception {
		when(oServ.getByStatus(Mockito.anyInt())).thenReturn(orderList);
		
		mock.perform(get("/orders/statusorders/{orderstatusid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.[0].orderId", is(order.getOrderId())))
		.andExpect(jsonPath("$.[0].subtotal", is(order.getSubtotal())))
		.andExpect(jsonPath("$.[1].orderId", is(order2.getOrderId())))
		.andExpect(jsonPath("$.[1].subtotal", is(order2.getSubtotal())));
	}
	
	@Test
	public void testGetOrderByStatusAndCustomer() throws Exception {
		when(oServ.getByStatusAndCustomer(Mockito.anyInt())).thenReturn(orderList);
		
		mock.perform(get("/orders/neworders/{customerid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.orderId", is(order.getOrderId())))
		.andExpect(jsonPath("$.subtotal", is(order.getSubtotal())));
	}
	
	@Test
	public void testGetOrderById() throws Exception{
		when(oServ.getById(Mockito.anyInt())).thenReturn(order);
		
		mock.perform(get("/orders/individualorders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.orderId", is(order.getOrderId())))
		.andExpect(jsonPath("$.subtotal", is(order.getSubtotal())));
	}
	
	@Test
	public void testDeleteOrderById() throws Exception{
		mock.perform(delete("/orders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$").value("Order is Deleted"));
	}
	
	@Test
	public void testUpdateOrderTotal() throws Exception {
		
		mock.perform(put("/orders/addTotal").contentType(MediaType.APPLICATION_JSON)
				.content("{\"orderId\":\"1\",\"subtotal\":\"2\",\"tax\":\"1\",\"total\":\"3\"}"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$").value("Totals were updated"));
	}
	
	@Test
	public void testUpdateCanceledOrder() throws Exception{		
		mock.perform(post("/orders/canceledorders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testUpdateDeliveringOrder() throws Exception{
		mock.perform(post("/orders/deliveryorders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$").value("Order is Delivering"));
	}
	
	@Test
	public void testUpdateCompletedOrder() throws Exception{
		mock.perform(post("/orders/completedorders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$").value("Order is Completed"));
	}
	
	@Test
	public void testUpdateReadyOrder() throws Exception{
		mock.perform(post("/orders/readyorders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$").value("Order is Ready"));
	}
	
	@Test
	public void testUpdatePreparingOrder() throws Exception{
		mock.perform(post("/orders/preparingorders/{orderid}", order.getOrderId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$").value("Order is Preparing"));
	}
	
	@Test
	public void testInsertNewOrder() throws Exception{
		mock.perform(post("/orders/neworders/{customerid}", order.getOrderId()))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$").value("New Order was created"));
	}

}
