package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.CustomerController;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.service.CustomerService;

@SpringBootTest
public class CustomerControllerUnitTest {
	
	private String encrypted = "S15nC0XLiN9N9fJJR/Im05HUlRZcwbfw";
	private String encryptedCC1 = "MdQyoeUwysSodpzU7kgEMyDqUV5cFAIw159iJwafVZQ=";
	
	@Mock
	private CustomerService crServ;
	
	@InjectMocks
	private CustomerController crCon;
	
	private Customer customer;
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp(){
		customer = new Customer(1, "email@email.com", encrypted, "Daniel", "Butcher", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
		mock = MockMvcBuilders.standaloneSetup(crCon).build();
//		when(crServ.viewByEmail("email@email.com")).thenReturn(customer);
		when(crServ.getById(1)).thenReturn(customer);
		when(crServ.checkCreditCard( encryptedCC1, "Daniel Butcher", 999, 1, 2025, 0)).thenReturn("accepted");
		when(crServ.retrievePassword("email@email.com", "Daniel", "Butcher")).thenReturn("Email sent");
		doNothing().when(crServ).insertCustomer("email@email.com", encrypted, "Daniel", "Butcher", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
		doNothing().when(crServ).updateAddress("email@email.com", encrypted, "address2", "city2", "state2", "zipCode2");
		doNothing().when(crServ).updateCreditCard("email@email.com", encrypted, encryptedCC1, "Daniel Butcher", 999, 1, 2025);
		doNothing().when(crServ).deleteCustomer("email@email.com", encrypted);
	}
	
	@Test
	public void testCreateCustomer() throws Exception{
		mock.perform(post("/customer/register").contentType(MediaType.APPLICATION_JSON).content(
			"{ "
			+ "\"email\": \"testing3@test.com\", "
			+ "\"password\": \"password\", "
			+ "\"firstName\": \"firstName3\", "
			+ "\"lastName\": \"lastName3\", "
			+ "\"address\": \"address3\", "
			+ "\"city\": \"city3\", "
			+ "\"state\": \"state3\", "
			+ "\"zipCode\": \"12343\", "
			+ "\"phone\": \"123451233\", "
			+ "\"creditCardNumber\": \"123123123123\", "
			+ "\"creditCardName\": \"creditCardNameTest3\", "
			+ "\"creditCardCode\": 122, "
			+ "\"creditCardMonth\": 3, "
			+ "\"creditCardYear\": 2023 "
			+ " }"
		)).andExpect(status().isCreated()).andExpect(jsonPath("$").value("Customer account created"));
	}
	
	@Test
	public void testUpdateAddress() throws Exception{
		mock.perform(post("/customer/address").contentType(MediaType.APPLICATION_JSON).content(
			"{ "
			+ "\"email\": \"testing3@test.com\", "
			+ "\"password\": \"password\", "
			+ "\"address\": \"address4\", "
			+ "\"city\": \"city4\", "
			+ "\"state\": \"state4\", "
			+ "\"zipCode\": \"12344\" "
			+ " }"
		)).andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Customer address updated"));
	}
	
	@Test
	public void testUpdateCreditCard() throws Exception{
		mock.perform(post("/customer/creditcard").contentType(MediaType.APPLICATION_JSON).content(
			"{ "
			+ "\"email\": \"testing3@test.com\", "
			+ "\"password\": \"password\", "
			+ "\"creditCardNumber\": \"123123123124\", "
			+ "\"creditCardName\": \"creditCardNameTest4\", "
			+ "\"creditCardCode\": 124, "
			+ "\"creditCardMonth\": 4, "
			+ "\"creditCardYear\": 2024 "
			+ " }"
		)).andExpect(status().isAccepted()).andExpect(jsonPath("$").value("Customer credit card updated"));
	}
	
	/*
	@Test
	public void testGetAccountByEmail() throws Exception{
		mock.perform(get("/customer/{email}", customer.getEmail())).andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName", is(customer.getFirstName())))
		.andExpect(jsonPath("$.lastName", is(customer.getLastName())));
	}
	*/
	
	@Test
	public void testGetAccountById() throws Exception{
		mock.perform(get("/customer/account/{id}", customer.getCustomerId())).andExpect(status().isOk())
		.andExpect(jsonPath("$.email", is(customer.getEmail())))
		.andExpect(jsonPath("$.firstName", is(customer.getFirstName())));
	}
	
	@Test
	public void testRemoveCustomer() throws Exception{
		mock.perform(delete("/customer/removeAccount").contentType(MediaType.APPLICATION_JSON).content(
			"{ "
			+ "\"email\":\"email@email.com\", "
			+ "\"password\":\"password\" "
			+ " }"
		)).andExpect(status().isOk()).andExpect(jsonPath("$").value("Customer account deleted"));
	}
	
	@Test
	public void testVerifyCC() throws Exception{
		mock.perform(post("/customer/verifycreditcards").contentType(MediaType.APPLICATION_JSON).content(
			"{ "
			+ "\"creditCardNumber\": \" " + encryptedCC1 + "\", "
			+ "\"creditCardName\": \"Daniel Butcher\", "
			+ "\"creditCardCode\": 999, "
			+ "\"creditCardMonth\": 1, "
			+ "\"creditCardYear\": 2025, "
			+ "\"total\":0"
			+ " }"
		)).andExpect(status().isOk());
	}
	
	@Test
	public void testRetrievePassword() throws Exception{
		mock.perform(post("/customer/retrievepassword").contentType(MediaType.APPLICATION_JSON).content(
			"{ "
			+ "\"email\": \"email@email.com\", "
			+ "\"firstName\": \"Daniel\", "
			+ "\"lastName\": \"Butcher\" "
			+ " }"
		)).andExpect(status().isOk()).andExpect(jsonPath("$").value("Email sent"));
	}
}
