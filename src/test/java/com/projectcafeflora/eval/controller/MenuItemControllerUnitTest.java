package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectcafeflora.controller.MenuItemController;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.service.MenuItemService;

@SpringBootTest
public class MenuItemControllerUnitTest {
	
	@Mock
	private MenuItemService miServ;
	
	@InjectMocks
	private MenuItemController miCon;
	
	private MenuItem menuItem;
	private MenuItem menuItem2;
	private List<MenuItem> miList = new ArrayList<MenuItem>();
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception {
		menuItem = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "url1");
		menuItem2 = new MenuItem(2,"Bacon Cheese Burger", "Juicy bacon cheese burger with all the fixin's, fries included", 9.50, "url2");
		miList.add(menuItem);
		miList.add(menuItem2);
		mock = MockMvcBuilders.standaloneSetup(miCon).build();
		when(miServ.getById(1)).thenReturn(menuItem);
		when(miServ.getAll()).thenReturn(miList);
		doNothing().when(miServ).insertMenuItem(menuItem);
	}
	
	@Test
	public void getMenuItemByIdSuccess() throws Exception {
		mock.perform(get("/menu-items/individualmenuitems/{menuitemid}", menuItem.getItemId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.itemId", is(menuItem.getItemId())))
		.andExpect(jsonPath("$.itemName", is(menuItem.getItemName())))
		.andExpect(jsonPath("$.itemDescription", is(menuItem.getItemDescription())))
		.andExpect(jsonPath("$.itemCost", is(menuItem.getItemCost())))
		.andExpect(jsonPath("$.itemImageUrl", is(menuItem.getItemImageUrl())));
	}
	
	@Test
	public void getAllMenuItemSuccess() throws Exception {
		mock.perform(get("/menu-items/allmenuitems"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.[0].itemId", is(menuItem.getItemId())))
		.andExpect(jsonPath("$.[0].itemName", is(menuItem.getItemName())))
		.andExpect(jsonPath("$.[0].itemDescription", is(menuItem.getItemDescription())))
		.andExpect(jsonPath("$.[0].itemCost", is(menuItem.getItemCost())))
		.andExpect(jsonPath("$.[0].itemImageUrl", is(menuItem.getItemImageUrl())))
		.andExpect(jsonPath("$.[1].itemId", is(menuItem2.getItemId())))
		.andExpect(jsonPath("$.[1].itemName", is(menuItem2.getItemName())))
		.andExpect(jsonPath("$.[1].itemDescription", is(menuItem2.getItemDescription())))
		.andExpect(jsonPath("$.[1].itemCost", is(menuItem2.getItemCost())))
		.andExpect(jsonPath("$.[1].itemImageUrl", is(menuItem2.getItemImageUrl())));
	}
	
	@Test
	public void insertMenuItemSuccess() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		
		try {
		  json = mapper.writeValueAsString(menuItem);
		} catch (JsonProcessingException e) {
		   e.printStackTrace();
		}
		mock.perform(post("/menu-items").contentType(MediaType.APPLICATION_JSON)
		.content(json))
		.andExpect(status().isCreated()).andExpect(jsonPath("$").value("Menu Item was created"));
	}
	
	
	
	
}
