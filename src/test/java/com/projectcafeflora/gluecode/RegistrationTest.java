package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;

import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.RegistrationPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegistrationTest {

	public LoginPage loginPage;
	public RegistrationPage registrationPage;
	
	
//	//First Scenario
//	@Given("a user is at the login page to redirect to register page")
//	public void a_user_is_at_the_login_page_to_redirect_to_register_page() {
//		this.loginPage = new LoginPage(DriverUtility.driver);
//		assertEquals(loginPage.getGreeting(), "Hello Customer");
//	}
//
//	@When("a click the register a account link")
//	public void a_click_the_register_a_account_link() {
//		this.loginPage.register();
//	}

//	@Then("the user gets redirected to the registration page")
//	public void the_user_gets_redirected_to_the_registration_page() throws InterruptedException {
//		this.registrationPage = new RegistrationPage(DriverUtility.driver);
//		TimeUnit.SECONDS.sleep(5);
//		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/register");
//	}
//	
//	//Second Scenario
//	@Given("a user is at the registration page ready to create an account")
//	public void a_user_is_at_the_registration_page_ready_to_create_an_account() throws InterruptedException {
//		this.registrationPage = new RegistrationPage(DriverUtility.driver);
//		TimeUnit.SECONDS.sleep(10);
//		assertEquals(registrationPage.getTitle(), "Registration");
//	}
	 
	//After login test Click Registration
	@When("a user inputs their first name {string}")
	public void a_user_inputs_their_first_name(String string) {
		this.registrationPage = new RegistrationPage(DriverUtility.driver);
		this.registrationPage.setFirstName(string);
	}

	@When("a user inputs their last name {string}")
	public void a_user_inputs_their_last_name(String string) {
		this.registrationPage.setLastName(string);
	}

	@When("a user inputs a new email {string}")
	public void a_user_inputs_a_new_email(String string) {
		this.registrationPage.setEmail(string);
	}

	@When("a user inputs a new password {string}")
	public void a_user_inputs_a_new_password(String string) {
		this.registrationPage.setPassword(string);
	}

	@When("a user inputs their address {string}")
	public void a_user_inputs_their_address(String string) {
		this.registrationPage.setAddress(string);
	}

	@When("a user inputs their city {string}")
	public void a_user_inputs_their_city(String string) {
		this.registrationPage.setCity(string);
	}

	@When("a user inputs their state {string}")
	public void a_user_inputs_their_state(String string) {
		this.registrationPage.setState(string);
	}

	@When("a user inputs their zipCode {string}")
	public void a_user_inputs_their_zip_code(String string) {
		this.registrationPage.setZipCode(string);
	}

	@When("a user inputs their phonenumber {string}")
	public void a_user_inputs_their_phonenumber(String string) {
		this.registrationPage.setPhone(string);
	}
	
	@When("a user inputs their credit card name {string}")
	public void a_user_inputs_their_credit_card_name(String string) {
		this.registrationPage.setCreditCardName(string);
	}

	@When("a user inputs their credit card number {string}")
	public void a_user_inputs_their_credit_card_number(String string) {
		this.registrationPage.setCreditCardNumber(string);
	}

	@When("a user inputs their credit card month {string}")
	public void a_user_inputs_their_credit_card_month(String string) {
		this.registrationPage.setCreditCardMonth(Integer.parseInt(string));
	}

	@When("a user inputs their credit card year {string}")
	public void a_user_inputs_their_credit_card_year(String string) {
		this.registrationPage.setCreditCardYear(Integer.parseInt(string));
		
	}

	@When("a user inputs their credit card code {string}")
	public void a_user_inputs_their_credit_card_code(String string) {
		this.registrationPage.setCreditCardCode(Integer.parseInt(string));
	}

	@When("then the user sumbit the form")
	public void then_the_user_sumbit_the_form() throws InterruptedException {
		this.registrationPage.clickRegister();
		TimeUnit.SECONDS.sleep(10);
		DriverUtility.driver.switchTo().alert().accept();
	}
	
//	@When("click alert OK")
//	public void click_alert_OK() {
//		Alert alert = DriverUtility.driver.switchTo().alert();
//		alert.accept();
//	}
	
	@Then("the user gets redirected to the home page")
	public void the_user_gets_redirected_to_home_page() throws InterruptedException {
//		
		this.loginPage = new LoginPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(10);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/login");
	}
	
	//Second Scenario
	@When("a user clicked the login link at the buttom of the page")
	public void a_user_clicked_the_login_link_at_the_buttom_of_the_page() {
		this.registrationPage = new RegistrationPage(DriverUtility.driver);
		this.registrationPage.goBackToLogin();
	}

	@Then("the user gets redirected to the login page again")
	public void the_user_gets_redirected_to_the_login_page_again() {
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/login");
	}

}
