package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.projectcafeflora.eval.page.CartPage;
import com.projectcafeflora.eval.page.CheckOutPage;
import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.MenuPage;
import com.projectcafeflora.eval.page.PaymentPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CheckoutTest {
	
	public LoginPage loginPage;
	public HomePage homePage;
	public MenuPage menuPage;
	public CartPage cartPage;
	public CheckOutPage checkoutPage;
	public PaymentPage paymentPage;
	public WebDriver driver = new ChromeDriver();
	
	@When("a user clicks the menu button")
	public void a_user_clicks_the_menu_button() throws InterruptedException {
		this.homePage = new HomePage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/home");
		this.homePage.clickMenuButton();
	}
	
	@When("a user clicks the add burrito button twice")
	public void a_user_clicks_the_add_burrito_button_twice() throws InterruptedException {
		this.menuPage = new MenuPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/menu");
		this.menuPage.clickAddBurritoButton();
		TimeUnit.SECONDS.sleep(3);
		this.menuPage.clickAddBurritoButton();
		TimeUnit.SECONDS.sleep(3);
	}
	
	@When("then a user clicks go to cart")
	public void then_a_user_clicks_go_to_cart() throws InterruptedException {
		TimeUnit.SECONDS.sleep(6);
		this.menuPage.clickGoToCartButton();
//		this.menuPage.clickNavbarCartButton();
	}
	
	@When("then a user clicks checkout")
	public void then_a_user_clicks_checkout() throws InterruptedException {
		this.cartPage = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/cart");
		this.cartPage.clickCheckoutButton();
		TimeUnit.SECONDS.sleep(3);
		this.checkoutPage = new CheckOutPage(DriverUtility.driver);
		assertEquals(checkoutPage.getPageHeader(), "Customer Checkout");
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/checkout");
		assertEquals(this.checkoutPage.getSubtotal(), "$7.00");
		assertEquals(this.checkoutPage.getTax(), "$0.35");		
		assertEquals(this.checkoutPage.getTotal(), "$7.35");
	}
	
	@When("a user inputs their coupon code {string}")
	public void a_user_inputs_their_coupon_code(String string) throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		this.checkoutPage.setCoupon(string);
		TimeUnit.SECONDS.sleep(6);
		assertEquals(this.checkoutPage.getDiscount(), "-$1.75");
		assertEquals(this.checkoutPage.getTax(), "$0.26");
		assertEquals(this.checkoutPage.getTotal(), "$5.51");
	}
	
	@When("a user enters a tip {string}")
	public void a_user_enters_a_tip(String string) throws InterruptedException {
	    this.checkoutPage.setTip(string);
	    TimeUnit.SECONDS.sleep(3);
	    assertEquals(this.checkoutPage.getTip(), "$2.00");
	    assertEquals(this.checkoutPage.getTotal(), "$7.51");
	}
	
	@When("a user clicks pickup")
	public void a_user_clicks_pickup() throws InterruptedException {
	    this.checkoutPage.clickPickup();
	    TimeUnit.SECONDS.sleep(3);
	    assertEquals(this.checkoutPage.getTip(), "$0.00");
		assertEquals(this.checkoutPage.getTotal(), "$5.51");
	}
	
	@When("a user clicks delivery")
	public void a_user_clicks_delivery() throws InterruptedException {
	    this.checkoutPage.clickDelivery();
	    TimeUnit.SECONDS.sleep(3);
	}
	
	@When("a user enters a new tip {string}")
	public void a_user_enters_a_new_tip(String string) throws InterruptedException {
	    this.checkoutPage.setTip(string);
	    TimeUnit.SECONDS.sleep(3);
	    assertEquals(this.checkoutPage.getTip(), "$2.00");
	    assertEquals(this.checkoutPage.getTotal(), "$7.51");
	}
	
	@When("then a user submits to payment")
	public void then_a_user_submits_to_payment() {
	    this.checkoutPage.clickProceedPayment();
	}
	
	@Then("the user is redirected to the payment screen")
	public void the_user_is_redirected_to_the_payment_screen() throws InterruptedException {
		this.paymentPage = new PaymentPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/payment");
	}
	
	@When("a user inputs their wrong coupon code {string}")
	public void a_user_inputs_their_wrong_coupon_code(String string) throws InterruptedException {
		this.checkoutPage = new CheckOutPage(DriverUtility.driver);
		assertEquals(checkoutPage.getPageHeader(), "Customer Checkout");
		TimeUnit.SECONDS.sleep(3);
		this.checkoutPage.setCoupon(string);
		TimeUnit.SECONDS.sleep(6);
		assertEquals(this.checkoutPage.getDiscount(), "$0.00");
		assertEquals(this.checkoutPage.getTax(), "$0.35");
		assertEquals(this.checkoutPage.getTotal(), "$7.35");
	}
	
	@When("then a user clicks go back to cart")
	public void then_a_user_clicks_go_back_to_cart() {
	    this.checkoutPage.clickGoToCart();
	}
	
	@Then("the user is redirected back to the cart screen")
	public void the_user_is_redirected_back_to_the_cart_screen() throws InterruptedException {
		this.cartPage = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/cart");
	}
}
